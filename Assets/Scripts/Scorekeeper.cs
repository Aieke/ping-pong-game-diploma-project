using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Scorekeeper : MonoBehaviour
{
    public int ScoreLimit = 10;

    public TextMeshProUGUI Player1ScoreDisplay;

    public TextMeshProUGUI Player2ScoreDisplay;

    private int p1Score = 0;

    private int p2Score = 0;

    public void AddScore(int player)
    {
        if (player == 1)
        {
            p1Score++;
        }
        else if (player == 2)
        {
            p2Score++;
        }

        if (p1Score >= ScoreLimit || p2Score >= ScoreLimit)
        {
            if (p1Score > p2Score)
                Debug.Log("Player 1 wins");

            if (p2Score > p1Score)
                Debug.Log("Player 2 wins");
            else
                Debug.Log("Players are tied");

            p1Score = 0;
            p2Score = 0;

            Player1ScoreDisplay.text = p1Score.ToString();

            Player2ScoreDisplay.text = p2Score.ToString();
        }
    }
}
